# roll

A dice roller written in Rust

## Build

`cargo build --release`

## Usage

`./roll 2d12`

Input must be in the "d" notation so xdy, where y is the number of faces on the di
and x is the number of that type of di to roll. Output is summed, so 2d12 will roll
2 d12 di, sum the results, and return it. Eventually I'll get this so you can specify
a verbose output to show each di rolled.

## TODO
- [ ] - incorporate some arg parsing

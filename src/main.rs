use rand::{thread_rng, Rng};
use std::env;


fn main() {
    let rolls: Vec<String> = env::args()
        .skip(1)
        .collect();

    for roll in rolls {
        let i: Vec<u32> = roll.split("d")
            .map(|x| x.parse().unwrap_or(1))
            .collect();
        let mut rng = thread_rng();
        let mut summed: u32 = 0;
        //println!("Rolling {} d{}...", i[0], i[1]);
        for _di in 0..i[0] {
            let rolled: u32 = rng.gen_range(1, i[1] + 1);
            //println!("{}", rolled);
            summed += rolled;
        }
        //println!("Total: {}", summed);
        println!("{}", summed);
    }
}
